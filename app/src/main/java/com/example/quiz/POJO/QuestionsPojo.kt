package com.example.quiz.POJO

data class QuestionsPojo(

    val questionId: Int,
    val question: String,
    val option1: String,
    val option2: String,
    val option3: String,
    val answer: Int

)