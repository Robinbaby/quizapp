package com.example.quiz

import com.example.quiz.POJO.QuestionsPojo

object Questions { //singleton


    fun getQuestions(): ArrayList<QuestionsPojo> {

        val qList = ArrayList<QuestionsPojo>()

        val q1 = QuestionsPojo(
            1, "Which state people speaks malayalam?", "Tamil Nadu", "Andhra", "Kerala", 3)
        qList.add(q1)


        val q2= QuestionsPojo(2,"PM of India","Modhi","Sachin",
            "Rahul",1)
        qList.add(q2)


        val q3= QuestionsPojo(3,"Petrol Price Today","1/-","Above 100/-",
            "Below 50/-",2)
        qList.add(q3)


        val q4= QuestionsPojo(4,"Find Odd one","Chair","Tv",
            "Radio",1)
        qList.add(q4)

        return qList
    }

}