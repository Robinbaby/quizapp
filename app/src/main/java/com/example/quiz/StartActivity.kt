package com.example.quiz

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.quiz.POJO.QuestionsPojo
import com.example.quiz.databinding.ActivityStartBinding

class StartActivity : AppCompatActivity() {

    private lateinit var binding: ActivityStartBinding
    private val selectedAnswerList = ArrayList<Int>()
    private var selectedAnswer: Int = 0
    private var question:QuestionsPojo?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityStartBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val currentPos = 1
        question = Questions.getQuestions().get(currentPos - 1)
        val questionCount = Questions.getQuestions().size
        binding.questionTv.text = question!!.question
        binding.answer1btn.text = question!!.option1
        binding.answer2btn.text = question!!.option2
        binding.answer3btn.text = question!!.option3
        binding.progressBar.progress = currentPos * 25
        binding.questionStatusTv.text = "$currentPos" + "/" + questionCount

        binding.answer1btn.setOnClickListener {
            buttonSelected(binding.answer1btn)
           selectedAnswer=1

        }
        binding.answer2btn.setOnClickListener {
            buttonSelected(binding.answer2btn)
            selectedAnswer=2
        }
        binding.answer3btn.setOnClickListener {
            buttonSelected(binding.answer3btn)
            selectedAnswer=3
        }
        binding.answerSubmitBtn.setOnClickListener {
            setButtonsDefaultColors()
            answerButtonColorChange(question!!.answer,"#FF018786")

            if (selectedAnswer != question!!.answer) {
                answerButtonColorChange(selectedAnswer,"#CF1B0E")
            }

        }
    }

   /* private fun setAnswer(btn: Button, answer: Int) {
        if (selectedAnswer != question!!.answer) {
            btn.setBackgroundColor(Color.parseColor("#CF1B0E"))
        }
    }*/

    private fun answerButtonColorChange(answer: Int,colorCode: String) {
        when (answer) {

            1 -> {
                binding.answer1btn.setBackgroundColor(Color.parseColor(colorCode))
            }
            2 -> {
                binding.answer2btn.setBackgroundColor(Color.parseColor(colorCode))
            }
            3 -> {
                binding.answer3btn.setBackgroundColor(Color.parseColor(colorCode))
            }

        }
    }


    private fun buttonSelected(button: Button) {

        setButtonsDefaultColors()
        button.setBackgroundColor(Color.parseColor("#000000"))

    }

    private fun setButtonsDefaultColors() {
        binding.answer3btn.setBackgroundColor(Color.parseColor("#CDDC39"))
        binding.answer2btn.setBackgroundColor(Color.parseColor("#CDDC39"))
        binding.answer1btn.setBackgroundColor(Color.parseColor("#CDDC39"))

    }
}