package com.example.quiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.quiz.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        var userName = binding.userNameET.text

        binding.startBtn.setOnClickListener {
            val intent = Intent(this, StartActivity::class.java)
            intent.putExtra("userName", userName)
            if (userName.isEmpty()) {
                Toast.makeText(this, "Enter Name", Toast.LENGTH_LONG).show()
            } else {
                startActivity(intent)
                finish()
            }
        }
    }
}